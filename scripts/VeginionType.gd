
var scene setget ,get_scene
var type setget , get_type
var followers = [] setget set_veginions, get_veginions

func _init(_scene, _type):
	scene = _scene
	type = _type

func set_veginions(veg):
	followers = veg

func get_veginions():
	return followers

func get_scene():
	return scene

func get_type():
	return type