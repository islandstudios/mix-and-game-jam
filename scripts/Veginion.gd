extends KinematicBody2D

#=============== Memeber Data ===============#


export (int) var max_fall_speed = 150
export (int) var max_speed = 45
export (int) var move_speed = 8
export (int) var type setget ,get_type

var player
var state_machine
var sprite
var velocity = Vector2.ZERO
var gravity = 8
var max_pop_speed = 100
var max_air_speed = 150
var friction = 0.2
var air_friction = 0.1
var following = false
var follow_dist = 0
var follow_threshold = 3
var grab_radius = 20

#============= Helper Functions =============#
func get_type():
	return type

func set_velocity(vel :Vector2):
	velocity = vel

func set_follow(follow :bool):
	following = follow

func set_follow_dist(dist :int):
	follow_dist = dist

func move():
	var destination
	if(!following):
		destination = player.global_position.x
	else:
		if(player.facing_right()):
			destination = player.global_position.x - follow_dist
		else:
			destination = player.global_position.x + follow_dist

	var distance = (global_position - Vector2(destination, global_position.y)).length()
	var dist_from_player = (global_position - player.global_position).length()

	if(distance > follow_threshold):
		if(global_position.x < destination):
			velocity.x += move_speed
		elif(global_position.x > destination):
			velocity.x -= move_speed
	elif(!following and dist_from_player <= grab_radius ):
		following = true
		player.add_veginion(self)

func get_state():
	return state_machine.get_current_node()

func set_state(state):
	state_machine.travel(state)

func apply_friction():
	var fric = air_friction
	if(is_on_floor()):
		fric = friction
	velocity.x = lerp(velocity.x, 0, fric)
	if(abs(velocity.x) < 1):
		velocity.x = 0

func apply_gravity():
	velocity.y += gravity

func set_direction():
	if(velocity.x > 0):
		sprite.set_flip_h(false)
	elif(velocity.x < 0):
		sprite.set_flip_h(true)

func get_thrown():
	if((global_position - player.global_position).length() <= grab_radius):
		set_state("fly")
		following = false
		var dir_val = -1 + (int(player.facing_right()) * 2) # 1 = right -1 = left
		global_position = player.global_position + Vector2(-dir_val * 4, -10)
		velocity = Vector2(dir_val * max_air_speed, -max_pop_speed)
	else:
		player.add_veginion(self)

#============== State Functions =============#

func idle():
	move()
	if(abs(velocity.x) > 0):
		set_state("run")

func run():
	move()
	if(velocity.x == 0):
		set_state("idle")

func fly():
	velocity.x = max_air_speed * sign(velocity.x)
	if(is_on_floor()):
		set_state("idle")

#============ Built In Functions ============#

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node("../Sally")
	sprite = $Sprite
	state_machine = $AnimationTree.get("parameters/playback")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	match(get_state()):
		"idle":
			idle()
		"run":
			run()
		"attack":
			pass
		"fly":
			fly()
		"death":
			pass
		"start_pickup":
			pass
		"pickup_struggle":
			pass
		"pickup":
			pass
		"carry":
			pass

	apply_gravity()
	apply_friction()
	set_direction()

	if(get_state() != "fly"):
		velocity.x = clamp(velocity.x, -max_speed, max_speed)
	else:
		velocity.x = clamp(velocity.x, -max_air_speed, max_air_speed)
	velocity.y = clamp(velocity.y, -max_pop_speed, max_fall_speed)
	move_and_slide(velocity * delta * 60, Vector2(0, -1))
