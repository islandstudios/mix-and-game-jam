extends Node2D

var plant_type = 0 setget set_plant, get_plant

func set_plant(plant):
	plant_type = plant

func get_plant():
	return plant_type
