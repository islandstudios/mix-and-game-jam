
var queue = [] setget set_queue, get_queue
var follow_dist = 8

func _init(_queue):
	queue = _queue

func set_queue(_queue):
	queue = _queue

func get_queue():
	return queue

func get_scene(type):
	return queue[type_index(type)].scene

func num_veginions():
	var count = 0
	for i in queue:
		count += len(i.followers)
	return count

func cycle_helper():
	queue.append(queue.pop_front())
	if(len(queue[0].followers) == 0):
		cycle_helper()

func cycle():
	if(num_veginions() > 0):
		cycle_helper()
		follow_dist_refresh()

func next_veg():
	var next = null
	while(next == null):
		var v = queue[0]
		if(len(v.followers) > 0):
			next = v.followers.pop_front()
			if(len(v.followers) == 0):
				cycle()
			follow_dist_refresh()
			return next
		else:
			cycle()

func type_index(type):
	var i = 0
	for q in queue:
		if(q.type == type):
			return i
		i += 1

func add(veginion):
	var i = type_index(veginion.type)
	if(queue[i].followers.find(veginion) == -1):
		queue[i].followers.append(veginion)
	follow_dist_refresh()

func follow_dist_refresh():
	var pos = 0
	for q in queue:
		for v in q.followers:
			v.set_follow_dist(follow_dist + pos)
			pos += 2