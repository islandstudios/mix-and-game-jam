extends KinematicBody2D

#=============== Memeber Data ===============#

var gravity = 10
var original_grav = gravity
var velocity = Vector2()
var move_speed = 10
var climb_speed = 50
var max_speed = 50
var max_fall_speed = 160
var friction = .2
var touching_ladder = false
var touching_farm = false
var facing_right = true
var plant_type = 0

#VegQueue set up
var VegQueue = load("res://scripts/VeginionQueue.gd")
var VegType = preload("res://scripts/VeginionType.gd")
var veginions = VegQueue.new([VegType.new(preload("res://scenes/Carrot.tscn"), 0), VegType.new(preload("res://scenes/Potato.tscn"), 1), VegType.new(preload("res://scenes/Onion.tscn"), 2)])

#nodes
#onready var veginion_scenes = [preload("res://scenes/Carrot.tscn"), preload("res://scenes/Potato.tscn"), preload("res://scenes/Onion.tscn")]
onready var state_machine = $AnimationTree.get("parameters/playback")
onready var sprite = $Sprite
onready var prompt = $prompt
var farm_state_machine
var farm
var to_throw

#inputs
var left = false
var right = false
var up = false
var down = false
var action = false
var cycle = false
var call = false

#============= Helper Functions =============#

# warning-ignore:function_conflicts_variable
func facing_right():
	return facing_right

func add_veginion(veginion):
	veginions.add(veginion)

func get_inputs():
	left = Input.is_action_pressed("left")
	right = Input.is_action_pressed("right")
	up = Input.is_action_pressed("up")
	down = Input.is_action_pressed("down")
	action = Input.is_action_just_pressed("action")
	cycle = Input.is_action_just_pressed("send")
	call = Input.is_action_just_pressed("call")
	var areas = $ladder_box.get_overlapping_areas()
	touching_ladder = false
	for area in areas:
		if(area.is_in_group("ladder")):
			touching_ladder = true
	areas = $Sprite/farm_box.get_overlapping_areas()
	touching_farm = false
	for area in areas:
		if(area.is_in_group("farm_land")):
			touching_farm = true
			farm = area.get_node("../../")
			farm_state_machine = farm.get_node("AnimationTree").get("parameters/playback")
			break

func move():
	velocity.x += (int(right) - int(left)) * move_speed

func get_state():
	return state_machine.get_current_node()

func set_state(state):
	state_machine.travel(state)

func apply_friction():
	velocity.x = lerp(velocity.x, 0, friction)
	if(abs(velocity.x) < 1):
		velocity.x = 0

func apply_gravity():
	velocity.y += gravity

func set_direction():
	if(velocity.x > 0):
		sprite.set_flip_h(false)
		sprite.set_offset(Vector2.ZERO)
		sprite.get_node("farm_box").position = Vector2(3.5, 8)
		facing_right = true
	elif(velocity.x < 0):
		sprite.set_flip_h(true)
		sprite.set_offset(Vector2(-8, 0))
		sprite.get_node("farm_box").position = Vector2(-11, 8)
		facing_right = false

func camera_controller():
	var camera_speed = 0.1
	if(!(is_on_floor() || get_state() == "climb")):
		$Camera2D.set_offset(lerp($Camera2D.get_offset(), Vector2(0, 6), camera_speed))
	else:
		$Camera2D.set_offset(lerp($Camera2D.get_offset(), Vector2(0, -18), camera_speed))

func ladder_change():
	if(touching_ladder && (up || down)):
		set_state("climb")

func farming():
	prompt.visible = false
	if(touching_farm):
		var farm_state = farm_state_machine.get_current_node().lstrip("1234567890_")
		if(farm_state == "idle"):
			prompt.visible = true 
			prompt.set_frame(plant_frame(0, false))
			if(action):
				set_state("plant")
			if(cycle):
				cycle_plant_type()
		elif(farm_state == "seeded"):
			prompt.visible = true 
			prompt.set_frame(plant_frame(1, true))
			if(action):
				set_state("water")
		elif(farm_state == "grown"):
			prompt.visible = true 
			prompt.set_frame(plant_frame(2, true))
			if(action):
				set_state("pick")
		return true
	return false

func finished_plant():
	farm.plant_type = plant_type
	farm_state_machine.travel(str(farm.plant_type) + "_seeded")

func finished_water():
	farm_state_machine.travel(str(farm.plant_type) + "_growing")

func finished_pluck():
	farm_state_machine.travel("idle")
	var veginion = veginions.get_scene(farm.plant_type).instance() #WHERE WE INSTANCE NEW VEGINION
	veginion.position = get_node("Sprite/farm_box/CollisionShape2D").global_position + Vector2(0, -5)
	veginion.set_velocity(Vector2(0, -100))
	get_parent().add_child(veginion)

func throw_veg():
	if(action):
		set_state("throw")
		if(veginions.num_veginions() > 0):
			to_throw = veginions.next_veg()
		else:
			to_throw = null

func throw():
	if(to_throw != null):
		to_throw.get_thrown()

func cycle_plant_type():
	plant_type = (plant_type + 1) % len(veginions.queue)

func plant_frame(index, get_farm):
	if(get_farm):
		return index + (farm.plant_type * len(veginions.queue))
	else:
		return index + (plant_type * len(veginions.queue))

func cycle():
	if(cycle):
		veginions.cycle()

#============== State Functions =============#

func idle():
	move()
	if(abs(velocity.x) > 0):
		set_state("run")
	ladder_change()
	if(!farming()):
		throw_veg()
		cycle()

func run():
	move()
	if(velocity.x == 0):
		set_state("idle")
	ladder_change()
	if(!farming()):
		throw_veg()
		cycle()

func climb():
	move()
	if(touching_ladder):
		gravity = 0
		if(up):
			velocity.y = -climb_speed
		elif(down):
			velocity.y = climb_speed
		else:
			velocity.y = 0
		
	else:
		gravity = original_grav
		set_state("idle")

func plant():
	pass

func water():
	pass

func pick():
	pass

func call_veg():
	pass

# warning-ignore:function_conflicts_variable
func send():
	pass

#============ Built In Functions ============#

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	get_inputs()
	
	var state = get_state()
	match(state):
		"idle":
			idle()
		"run":
			run()
		"climb":
			climb()
		"plant":
			plant()
		"water":
			water()
		"pick":
			pick()
		"throw":
			throw()
		"call":
			call_veg()
		"send":
			send()
	
	apply_friction()
	apply_gravity()
	set_direction()
	
	camera_controller()
	
	velocity.x = clamp(velocity.x, -max_speed, max_speed)
	velocity.y = clamp(velocity.y, -max_fall_speed, max_fall_speed)
# warning-ignore:return_value_discarded
	move_and_slide(velocity * delta * 60, Vector2(0, -1))
